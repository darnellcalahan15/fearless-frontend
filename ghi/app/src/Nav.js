import { NavLink } from "react-router-dom";

function Nav() {
    return (

      <nav class="navbar navbar-light bg-light">
        <div className="navbar">
        <NavLink className='nav-link' aria-current='page'
        to={'/'}
        >Home</NavLink>

      <NavLink className='nav-link' aria-current='page'
        to={'/conferences/new'}
        >Add Conference</NavLink>

      <NavLink className='nav-link' aria-current='page'
        to={'/locations/new'}
        >Add Locations</NavLink>

      <NavLink className='nav-link' aria-current='page'
        to={'/attendees'}
        >Attendees</NavLink>

    <NavLink className='nav-link' aria-current='page'
        to={'/attendees/new'}
        >Add Attendee</NavLink>
        </div>
    </nav>



    );
  }

  export default Nav;
