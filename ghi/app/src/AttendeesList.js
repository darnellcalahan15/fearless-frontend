import { useEffect, useState} from "react";

function AttendeesList(props) {
  const[attendees, setAttendee] = useState([])

  // if (attendees === undefined) {
  //   return null;
  // }

   const loadAttendees= async () => {
    const response = await fetch('http://localhost:8001/api/attendees/');
    if (response.ok) {
      const data = await response.json();
      setAttendee(data.attendees)
      console.log(data)


    }
  }
  useEffect(() => {
    loadAttendees();
  }, []);
    return (

    <table className="table table-striped">
      <thead>
        <tr>
          <th>Name</th>
          <th>Conference</th>
        </tr>
      </thead>
      <tbody>
      {attendees.map(attendee => {
  return (
    <tr key={attendee.href}>
      <td>{ attendee.name }</td>
      <td>{ attendee.conference }</td>
    </tr>
  );
})}

      </tbody>
    </table>
    );

}

export default AttendeesList;
