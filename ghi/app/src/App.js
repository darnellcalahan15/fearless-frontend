import Nav from './Nav';
import AttendeesList from './AttendeesList';
import LocationForm from './LocationForm';
import ConferenceForm from './ConferenceForm';
import { createBrowserRouter, Outlet, RouterProvider, useRouteError} from 'react-router-dom';
import AttendConferenceForm from './AttendConferenceForm';
import MainPage from './MainPage';


function App(props) {


  const router = createBrowserRouter([

    {
      path: '/', element: (
        <>
        <Nav />
        <div className='container'>
          <Outlet />
        </div>
        </>
      ),
      children: [
        { index: true, element: <MainPage /> },
        {
          path: '/attendees', element: <AttendeesList />
        },
        {
          path: '/conferences',
          children: [
            {path: 'new', element: <ConferenceForm />}
          ]
        },
        {
          path: "locations",
          children: [
            { path: "new", element: <LocationForm /> },
          ]
        },
        {
          path: '/attendees',
          children: [
            {path: 'new', element: <AttendConferenceForm />}
          ]
        },
      ]
    },

  ])

  return <RouterProvider router={router} />;
}

export default App;
